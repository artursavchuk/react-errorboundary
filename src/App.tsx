import React, { useState } from 'react'
import { Form } from './Form'
import { Starship } from './Starship'
import ErrorBoundary from './ErrorBoundary'

export const idList = [99, 5, 15]

export const App = () => {
  const [starship, setStarship] = useState<null | unknown>(null)
  const [starshipId, setStarshipId] = useState<null | number>(null)

  const handleResetError = () => {
    setStarship(null)
    setStarshipId(null)
  }

  return (
    <div className="app">
      <ErrorBoundary onResetError={handleResetError}>
        <Form
          starshipId={starshipId}
          onChangeId={(id) => setStarshipId(id)}
          onDataLoaded={(data) => setStarship(data)}
        />
        <Starship starship={starship} />
      </ErrorBoundary>
    </div>
  )
}
