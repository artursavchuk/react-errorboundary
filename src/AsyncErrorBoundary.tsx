import React, { useEffect, useState } from 'react'
import { ErrorComponent } from './ErrorComponent'

export const AsyncErrorBoundary = ({ children }) => {
  const [error, setError] = useState<null | Error>(null)

  const handleEvent = (event: PromiseRejectionEvent) => {
    setError(new Error(event.reason.message))
  }

  const handleResetError = () => setError(null)

  useEffect(() => {
    window.addEventListener('unhandledrejection', handleEvent)
    return () => {
      window.removeEventListener('unhandledrejection', handleEvent)
    }
  })

  if (error) {
    return <ErrorComponent error={error} onResetError={handleResetError} />
  }

  return children
}
