import React, { PropsWithChildren } from 'react'

type Props = {
  label: string | null
  onClick: () => void
}

export const Button = ({
  label,
  onClick,
  children,
}: PropsWithChildren<Props>) => {
  return (
    <div className="button-wrapper">
      <p>{label}</p>
      <button onClick={onClick}>{children}</button>
    </div>
  )
}
