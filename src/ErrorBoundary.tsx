import React, { ErrorInfo, PropsWithChildren } from 'react'
import { ErrorComponent } from './ErrorComponent'

type Props = {
  onResetError?: () => void
}

type State = {
  error: null | Error
}

class ErrorBoundary extends React.Component<PropsWithChildren<Props>, State> {
  constructor(props: PropsWithChildren) {
    super(props)
    this.state = { error: null }
    this.handleResetError = this.handleResetError.bind(this)
  }

  static getDerivedStateFromError(error: Error) {
    return { error: error }
  }

  handleResetError() {
    this.props.onResetError?.()
    this.setState({ error: null })
  }

  render() {
    if (this.state.error) {
      return (
        <ErrorComponent
          error={this.state.error}
          onResetError={this.handleResetError}
        />
      )
    }

    return this.props.children
  }
}

export default ErrorBoundary
