import React from 'react'

type Props = {
  error: Error
  onResetError?: () => void
}

export const ErrorComponent = ({ error, onResetError }: Props) => {
  return (
    <div className="error">
      <p>{error.message}</p>
      {onResetError && <button onClick={onResetError}>reset</button>}
    </div>
  )
}
