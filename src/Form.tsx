import React, { ChangeEvent } from 'react'
import { idList } from './App'
import { Button } from './Button'
import { isAsyncSuccess } from './helper'

type Props = {
  starshipId: null | number
  onChangeId: (id: number) => void
  onDataLoaded: (data: unknown) => void
}

export const Form = ({ starshipId, onChangeId, onDataLoaded }: Props) => {
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    onChangeId(+e.target.value)
  }

  const handleSubmit = async () => {
    const response = await fetch(`http://swapi.dev/api/starships/${starshipId}`)

    if (!response.ok) {
      throw new Error('Starship not found')
    }

    const starship = await response.json()
    starship.id = starshipId
    onDataLoaded(starship)
  }

  return (
    <div className="form">
      <div className="currentValue">
        {!starshipId
          ? 'Choose some option'
          : `Selected starship id: ${starshipId}`}
      </div>

      <div className="radios">
        {idList.map((id) => {
          return (
            <input
              type="radio"
              name="starshipId"
              key={id}
              value={id}
              checked={id === starshipId}
              onChange={handleChange}
            />
          )
        })}
      </div>

      <div className="buttons">
        <Button label="sync error" onClick={() => onDataLoaded({})}>
          get starship
        </Button>

        <Button
          label={isAsyncSuccess(starshipId) ? 'ok' : 'async error'}
          onClick={handleSubmit}
        >
          get starship
        </Button>
      </div>
    </div>
  )
}
