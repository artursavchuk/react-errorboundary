import React from 'react'

type Props = {
  starship: null | unknown
}

export const Starship = ({ starship }: Props) => {
  if (starship === null) return <></>

  const starshipName = starship.name.toUpperCase()

  return (
    <div className="starship">
      <img src={`/${starship.id}.jpg`} alt={starshipName} />
      <div className="name">{starshipName}</div>
    </div>
  )
}
