import { idList } from './App'

export const isAsyncSuccess = (starshipId: number | null) => {
  return (
    starshipId !== null &&
    (idList[1] === starshipId || idList[2] === starshipId)
  )
}
