import './style.scss'
import React from 'react'
import ReactDOM from 'react-dom/client'
import {App} from './App'
import { AsyncErrorBoundary } from './AsyncErrorBoundary'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <div className="background">
      <AsyncErrorBoundary>
        <App />
      </AsyncErrorBoundary>
    </div>
  </React.StrictMode>
)
